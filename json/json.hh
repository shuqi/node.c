/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <nodecpp/json/json.h>
#include <map>

namespace nodecppimpl { namespace json {

class json {
    friend class nodecpp::json::json;
public:
    json(std::initializer_list<std::tuple<std::string, nodecpp::json::json_value> >);
    json(const json& other);
    json(json&& other);
    json(std::initializer_list<nodecpp::json::json_value> array);
    json(std::initializer_list<long long> objs);
    json(std::initializer_list<double> objs);
    json(std::initializer_list<std::string> objs);
    json(std::initializer_list<const char*> objs);
    json(std::initializer_list<bool> objs);
    json(const std::vector<std::string> &strs);
    json();
    nodecpp::json::json_value& operator [](const std::string& key);
    nodecpp::json::json_value& operator [](unsigned int key);
    bool operator ==(const json& other) const;
    bool operator !=(const json& other) const;
    const std::map<std::string, nodecpp::json::json_value>& get_pairs() const
    {
        return _pairs;
    }
    bool is_array() const { return _is_array; }
public:
    void serialize(std::ostream& s) const;
    static json *parse(std::istream& in);
private: //parse
    typedef struct {
        size_t column;
        size_t line;
        std::string to_string();
    } parse_position;
    static json_value *parse_number(std::istream& in, parse_position& p);
    static std::string get_escaped(std::istream& in, parse_position& p);
    static std::string parse_string(std::istream& in, parse_position& p);
    static json_value *parse_value(std::istream& in, parse_position& p);
    static json *parse_array(std::istream& in, parse_position& p);
    static json *parse_object(std::istream& in, parse_position &);
private:
    std::map<std::string, nodecpp::json::json_value> _pairs;
    std::vector<nodecpp::json::json_value> _array;
    bool _is_array;
};

class json_value {
public: //virtual stuff
    virtual ~json_value();
    virtual void get_string(std::string&) const = 0;
    virtual long long get_int()const = 0;
    virtual double get_double() const = 0;
    virtual void get_json(json&) const = 0;
    virtual bool get_bool() const = 0;
    virtual json_value* copy() const = 0;
    virtual nodecpp::json::json_value::type get_type() const = 0;
    virtual const std::vector<nodecpp::json::json_value>& get_array() const = 0;
    virtual std::vector<nodecpp::json::json_value>& get_array() = 0;
    virtual void serialize(std::ostream& s) const = 0;
public:
    operator bool() { return get_bool(); }
};

class json_string : public json_value {
    friend class nodecpp::json::json_value;
public:
    json_string(const char* str) : _value(str) {}
    json_string(const std::string& str) : _value(str) {}
    json_string(std::string&& str) : _value(str) {}
    json_string() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::STRING;
    }
    void set_string(const std::string& str) { _value = str; }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    std::string _value;
};

class json_int : public json_value {
public:
    json_int(long long i) : _value(i) {}
    json_int() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    void set_int(long long val) { _value = val; }
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::INT;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    long long _value;
};

class json_bool : public json_value {
public:
    json_bool(bool i) : _value(i) {}
    json_bool() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    void set_bool(bool val) { _value = val; }
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::INT;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    bool _value;
};

class json_double : public json_value {
public:
    json_double(double d) : _value(d) {}
    json_double() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    void set_double(double val) { _value = val; }
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::DOUBLE;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    double _value;
};

class json_json : public json_value {
    friend class nodecpp::json::json_value;
public:
    json_json(const json& js) : _value(js) {}
    json_json(json&& js) : _value(js) {}
    json_json() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    void set_json(const json& val) { _value = val; }
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::JSON;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    json _value;
};

class json_array : public json_value {
public:
    json_array(std::vector<nodecpp::json::json_value>&& vec);
    json_array(const std::vector<nodecpp::json::json_value>& vec);
    json_array() = delete;
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::ARRAY;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
private:
    std::vector<nodecpp::json::json_value> _value;
};

class json_null : public json_value {
public:
    virtual void get_string(std::string &) const;
    virtual long long get_int() const;
    virtual bool get_bool() const;
    virtual double get_double() const;
    virtual void get_json(json&) const;
    virtual json_value* copy() const;
    virtual nodecpp::json::json_value::type get_type() const
    {
        return nodecpp::json::json_value::NULLPTR;
    }
    virtual const std::vector<nodecpp::json::json_value>& get_array() const;
    virtual std::vector<nodecpp::json::json_value>& get_array();
    virtual void serialize(std::ostream& s) const;
};

}}
