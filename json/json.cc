/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "json.hh"
#include "unmarshaller.hh"
#include <nodecpp/json/json.h>


std::tuple<std::string, nodecpp::json::json_value> operator <<(const std::string& k, const nodecpp::json::json_value& v)
{
    return std::tuple<std::string, nodecpp::json::json_value>(k, v);
}

std::ostream& operator <<(std::ostream& s, const nodecpp::json::json& j)
{
    j.serialize(s);
    return s;
}

namespace nodecpp { namespace json {

json_value::json_value() : _impl(new nodecppimpl::json::json_null()) {}
json_value::json_value(const char *str) : _impl(new nodecppimpl::json::json_string(str)) {}
json_value::json_value(const std::string &str) : _impl(new nodecppimpl::json::json_string(str)) {}
json_value::json_value(double d) : _impl(new nodecppimpl::json::json_double(d)) {}
json_value::json_value(bool b) : _impl(new nodecppimpl::json::json_bool(b)) {}
json_value::json_value(long long i) : _impl(new nodecppimpl::json::json_int(i)) {}
json_value::json_value(json &&js) : _impl(new nodecppimpl::json::json_json(*js._impl)) {}
json_value::json_value(const json &js) : _impl(new nodecppimpl::json::json_json(*js._impl)) {}
json_value::json_value(json_value &&other) : _impl(other._impl) { other._impl = 0; }
json_value::json_value(const json_value &other) : _impl(other._impl->copy()) {}
json_value::json_value(const std::vector<json_value> &arr) : _impl(new nodecppimpl::json::json_array(arr)) {}
json_value::json_value(nodecppimpl::json::json_value *impl) : _impl(impl) {}

json_value::~json_value() { delete _impl; }

json_value& json_value::operator =(const std::string& value) {
    if (_impl->get_type() == STRING) {
        dynamic_cast<nodecppimpl::json::json_string*>(_impl)->set_string(value);
    } else {
        delete _impl;
        _impl = new nodecppimpl::json::json_string(value);
    }
    return *this;
}

json_value& json_value::operator =(const char * str) {
    if (_impl->get_type() == STRING) {
        dynamic_cast<nodecppimpl::json::json_string*>(_impl)->set_string(str);
    } else {
        delete _impl;
        _impl = new nodecppimpl::json::json_string(str);
    }
    return *this;
}

json_value& json_value::operator =(long long val) {
    if (_impl->get_type() == INT) {
        dynamic_cast<nodecppimpl::json::json_int*>(_impl)->set_int(val);
    } else {
        delete _impl;
        _impl = new nodecppimpl::json::json_int(val);
    }
    return *this;
}

json_value& json_value::operator =(double val) {
    if (_impl->get_type() == DOUBLE) {
        dynamic_cast<nodecppimpl::json::json_double*>(_impl)->set_double(val);
    } else {
        delete _impl;
        _impl = new nodecppimpl::json::json_double(val);
    }
    return *this;
}

json_value& json_value::operator =(const json& value) {
    if (_impl->get_type() == JSON) {
        dynamic_cast<nodecppimpl::json::json_json*>(_impl)->set_json(*value._impl);
    } else {
        delete _impl;
        _impl = new nodecppimpl::json::json_json(*value._impl);
    }
    return *this;
}

json_value& json_value::operator =(const json_value& other)
{
    delete _impl;
    _impl = other._impl->copy();
}

bool json_value::operator ==(const json_value& other) const
{
    if (other._impl->get_type() != _impl->get_type())
        return false;
    switch (_impl->get_type()) {
    case INT:
        return _impl->get_int() == other._impl->get_int();
    case DOUBLE:
        return _impl->get_double() == other._impl->get_double();
    case STRING:
        return dynamic_cast<nodecppimpl::json::json_string*>(_impl)->_value ==
                dynamic_cast<nodecppimpl::json::json_string*>(other._impl)->_value;
    case NULLPTR:
        return true;
    case JSON:
        return dynamic_cast<nodecppimpl::json::json_json*>(_impl)->_value ==
                dynamic_cast<nodecppimpl::json::json_json*>(other._impl)->_value;
    }
}

bool json_value::operator ==(const std::string& other) const {
    std::string str;
    try {
        _impl->get_string(str);
        return str == other;
    } catch (wrong_type_exception&) {
        return false;
    }
}

bool json_value::operator ==(const char* other) const {
    return *this == std::string(other);
}

bool json_value::operator ==(const json& other) const {
    if (_impl->get_type() != JSON)
        return false;
    nodecppimpl::json::json c;
    _impl->get_json(c);
    return c == *other._impl;
}

bool json_value::operator ==(long long other) const {
    return _impl->get_type() == INT && _impl->get_int() == other;
}

bool json_value::operator ==(double other) const {
    return _impl->get_type() == DOUBLE && _impl->get_double() == other;
}

bool json_value::operator ==(bool other) const {
    return _impl->get_type() == BOOL && _impl->get_bool() == other;
}

bool json_value::operator ==(decltype(nullptr)) const {
    return _impl->get_type() == NULLPTR;
}

bool json_value::operator !=(const std::string& other) const {
    std::string str;
    try {
        _impl->get_string(str);
        return str != other;
    } catch (wrong_type_exception&) {
        return true;
    }
}

bool json_value::operator !=(const char* other) const {
    return *this != std::string(other);
}

bool json_value::operator !=(const json& other) const {
    if (_impl->get_type() != JSON)
        return true;
    nodecppimpl::json::json c;
    _impl->get_json(c);
    return c != *other._impl;
}

bool json_value::operator !=(const json_value& other) const {
    return !(*this == other);
}

bool json_value::operator !=(long long other) const {
    return _impl->get_type() != INT || _impl->get_int() != other;
}

bool json_value::operator !=(bool other) const {
    return _impl->get_type() != BOOL || _impl->get_bool() != other;
}

bool json_value::operator !=(double other) const {
    return _impl->get_type() != DOUBLE || _impl->get_double() != other;
}

bool json_value::operator !=(decltype(nullptr)) const {
    return _impl->get_type() != NULLPTR;
}

json_value::operator std::string()
{
    std::string res;
    get_string(res);
    return res;
}

json_value::operator long long()
{
    return get_int();
}

json_value::operator double()
{
    return get_double();
}

json_value::operator json()
{
    json res;
    get_json(res);
    return res;
}

json_value::operator bool()
{
    return get_bool();
}

void json_value::get_string(std::string &val) const
{
    _impl->get_string(val);
}

long long json_value::get_int() const
{
    return _impl->get_int();
}

double json_value::get_double() const
{
    return _impl->get_double();
}

void json_value::get_json(json &val) const
{
    nodecppimpl::json::json *js = new nodecppimpl::json::json();
    _impl->get_json(*js);
    val = json(js);
}

bool json_value::get_bool() const
{
    return _impl->get_bool();
}

auto json_value::get_type() const -> type
{
    return _impl->get_type();
}

const std::vector<json_value>& json_value::get_array() const
{
    return _impl->get_array();
}

std::vector<json_value>& json_value::get_array()
{
    return _impl->get_array();
}

void json_value::serialize(std::ostream &s) const
{
    _impl->serialize(s);
}

json::json(const nodecppimpl::json::json &other)
    : _impl(new nodecppimpl::json::json(other))
{}

json::json(nodecppimpl::json::json *other)
    : _impl(other)
{}

json::json() : _impl(new nodecppimpl::json::json()) {}

json::json(std::initializer_list<std::tuple<std::string, json_value> > objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(std::initializer_list<int> objs)
    : _impl(new nodecppimpl::json::json())
{
    for (auto i : objs) {
        _impl->_array.push_back(static_cast<long long>(i));
    }

}

json::json(std::initializer_list<long long> objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(std::initializer_list<double> objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(std::initializer_list<const char*> objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(std::initializer_list<bool> objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(std::initializer_list<std::string> objs)
    : _impl(new nodecppimpl::json::json(objs))
{
}

json::json(const std::vector<std::string> &strs)
    : _impl(new nodecppimpl::json::json(strs))
{
}

json::~json()
{
    delete _impl;
}

json& json::operator =(const json& other)
{
    delete _impl;
    _impl = new nodecppimpl::json::json(*unmarshaller<json, nodecppimpl::json::json>::get_const_implementation(other));
}

void json::serialize(std::ostream &stream) const
{
    _impl->serialize(stream);
}

json_value& json::operator [](const std::string& key) {
    (*_impl)[key];
}

json json::parse(std::istream &stream)
{
    return json(nodecppimpl::json::json::parse(stream));
}

}}

namespace nodecppimpl { namespace json {

json::json(std::initializer_list<std::tuple<std::string, nodecpp::json::json_value> > objs)
    : _is_array(false)
{
    for (auto i : objs) {
        _pairs[std::get<0>(i)] = std::get<1>(i);
    }
}


json::json(std::initializer_list<nodecpp::json::json_value> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}

json::json(std::initializer_list<bool> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}

json::json(std::initializer_list<long long> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}

json::json(std::initializer_list<double> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}

json::json(std::initializer_list<std::string> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}

json::json(std::initializer_list<const char*> array)
    : _is_array(true)
{
    for (auto i : array) {
        _array.push_back(i);
    }
}


json::json(const json &other)
    : _pairs(other._pairs), _array(other._array), _is_array(other._is_array)
{
}

json::json(json &&other)
    : _pairs(other._pairs), _array(other._array), _is_array(other._is_array)
{}

json::json(const std::vector<std::string> &strs)
    : _is_array(true)
{
    for (auto s : strs) {
        _array.push_back(s);
    }
}

json::json()
    : _is_array(false)
{}

nodecpp::json::json_value& json::operator [](const std::string& key) {
    if (_is_array && !_array.empty()) {
        throw nodecpp::json::wrong_type_exception("trying to query an array with a string as key");
    } else if (_array.empty())
        _is_array = false;
    return _pairs[key];
}

json_value::~json_value() {}

bool json::operator ==(const json& other) const {
    if (_is_array)
        return _array == other._array;
    else
        return _pairs == other._pairs;
}

bool json::operator !=(const json& other) const {
    if (_is_array)
        return _array != other._array;
    else
        return _pairs != other._pairs;
}

using namespace nodecpp::json;

double json_string::get_double() const
{
    throw wrong_type_exception("A value of type string can not be converted to double");
}

bool json_string::get_bool() const
{
    throw wrong_type_exception("A value of type string can not be converted to bool");
}

long long json_string::get_int() const
{
    throw wrong_type_exception("A value of type string can not be converted to double");
}

void json_string::get_json(json &) const
{
    throw wrong_type_exception("A value of type string can not be converted to json");
}

void json_string::get_string(std::string &value) const
{
    value = _value;
}

const std::vector<nodecpp::json::json_value>& json_string::get_array() const {
    throw wrong_type_exception("called get_array on string");
}

std::vector<nodecpp::json::json_value>& json_string::get_array() {
    throw wrong_type_exception("called get_array on string");
}

json_value* json_string::copy() const
{
    return new json_string(_value);
}

double json_bool::get_double() const
{
    return _value ? 1.0 : 0.0;
}

bool json_bool::get_bool() const
{
    return _value;
}

long long json_bool::get_int() const
{
    return _value ? 1 : 0;
}

void json_bool::get_json(json &) const
{
    throw wrong_type_exception("A value of type bool can not be converted to json");
}

void json_bool::get_string(std::string &value) const
{
    value = _value ? "1" : "0";
}

const std::vector<nodecpp::json::json_value>& json_bool::get_array() const {
    throw wrong_type_exception("called get_array on bool");
}

std::vector<nodecpp::json::json_value>& json_bool::get_array() {
    throw wrong_type_exception("called get_array on bool");
}

json_value* json_bool::copy() const
{
    return new json_bool(_value);
}

double json_int::get_double() const
{
    return _value;
}

bool json_int::get_bool() const
{
    return _value != 0 ? true : false;
}

long long json_int::get_int() const
{
    return _value;
}

void json_int::get_json(json &) const
{
    throw wrong_type_exception("A value of type int can not be converted to json");
}

void json_int::get_string(std::string &value) const
{
    value = std::to_string(_value);
}

const std::vector<nodecpp::json::json_value>& json_int::get_array() const {
    throw wrong_type_exception("called get_array on int");
}

std::vector<nodecpp::json::json_value>& json_int::get_array() {
    throw wrong_type_exception("called get_array on int");
}

json_value* json_int::copy() const
{
    return new json_int(_value);
}

double json_double::get_double() const
{
    return _value;
}

bool json_double::get_bool() const
{
    throw wrong_type_exception("A value of type double can not be converted to bool");
}

long long json_double::get_int() const
{
    throw wrong_type_exception("A value of type double can not be converted to int");
}

void json_double::get_json(json &) const
{
    throw wrong_type_exception("A value of type double can not be converted to json");
}

void json_double::get_string(std::string &value) const
{
    value = std::to_string(_value);
}

const std::vector<nodecpp::json::json_value>& json_double::get_array() const {
    throw wrong_type_exception("called get_array on double");
}

std::vector<nodecpp::json::json_value>& json_double::get_array() {
    throw wrong_type_exception("called get_array on double");
}

json_value* json_double::copy() const
{
    return new json_double(_value);
}

double json_json::get_double() const
{
    throw wrong_type_exception("A value of type json can not be converted to double");
}

bool json_json::get_bool() const
{
    throw wrong_type_exception("A value of type json can not be converted to bool");
}

long long json_json::get_int() const
{
    throw wrong_type_exception("A value of type json can not be converted to int");
}

void json_json::get_json(json &val) const
{
    val = _value;
}

void json_json::get_string(std::string &value) const
{
    throw wrong_type_exception("A value of type json can not be converted to string");
}

const std::vector<nodecpp::json::json_value>& json_json::get_array() const {
    throw wrong_type_exception("called get_array on json");
}

std::vector<nodecpp::json::json_value>& json_json::get_array() {
    throw wrong_type_exception("called get_array on json");
}

json_value* json_json::copy() const
{
    return new json_json(_value);
}

double json_array::get_double() const
{
    throw wrong_type_exception("An array can not be converted to double");
}

bool json_array::get_bool() const
{
    throw wrong_type_exception("An array can not be converted to bool");
}

long long json_array::get_int() const
{
    throw wrong_type_exception("An array can not be converted to int");
}

void json_array::get_json(json &val) const
{
    throw wrong_type_exception("An array can not be converted to json");
}

void json_array::get_string(std::string &value) const
{
    throw wrong_type_exception("An array can not be converted to string");
}

const std::vector<nodecpp::json::json_value>& json_array::get_array() const {
    return _value;
}

std::vector<nodecpp::json::json_value>& json_array::get_array() {
    return _value;
}

json_value* json_null::copy() const
{
    return new json_null();
}

json_array::json_array(const std::vector<nodecpp::json::json_value> &vec)
    : _value(vec) {}

json_array::json_array(std::vector<nodecpp::json::json_value>&& vec)
    : _value(vec) {}

double json_null::get_double() const
{
    throw wrong_type_exception("A null value can not be converted to double");
}

bool json_null::get_bool() const
{
    throw wrong_type_exception("A null value can not be converted to bool");
}

long long json_null::get_int() const
{
    throw wrong_type_exception("A null value can not be converted to int");
}

void json_null::get_json(json &val) const
{
    throw wrong_type_exception("A null value can not be converted to json");
}

void json_null::get_string(std::string &value) const
{
    throw wrong_type_exception("A null value can not be converted to string");
}

const std::vector<nodecpp::json::json_value>& json_null::get_array() const {
    throw wrong_type_exception("called get_array on null");
}

std::vector<nodecpp::json::json_value>& json_null::get_array() {
    throw wrong_type_exception("called get_array on null");
}

json_value* json_array::copy() const
{
    return new json_array(_value);
}

}}
