/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "json.hh"
#include <iostream>
#include <iomanip>

namespace nodecppimpl { namespace json {

static void serialize_array(std::ostream& out, const std::vector<nodecpp::json::json_value>& vec)
{
    out << "[ ";
    bool first = true;
    for (auto i : vec) {
        if (first)
            first = false;
        else
            out << ", ";
        i.serialize(out);
    }
    out << " ]";
}

static void serialize_string(std::ostream& out, const std::string& str)
{
    for (std::string::const_iterator i = str.begin(); i != str.end(); ++i) {
        unsigned char c = *i;
        if (c & 0x80) {
            out << "\\u";
            out << std::setw(2) << std::setfill('0') << std::hex << (int)c;
            ++i;
            c = *i;
            out << std::setw(2) << std::setfill('0') << std::hex << (int)c;
            continue;
        }
        switch (*i) {
        case '"':
            out << "\"";
            break;
        case '\t':
            out << "\\t";
            break;
        case '\r':
            out << "\\r";
            break;
        case '\b':
            out << "\\r";
            break;
        case '\f':
            out << "\\r";
            break;
        case '\\':
            out << "\\\\";
            break;
        default:
            out << *i;
        }
    }
}

void json::serialize(std::ostream &out) const
{
    if (_is_array) {
        serialize_array(out, _array);
        return;
    }
    bool first = true;
    out << "{";
    for (auto i : _pairs) {
        if (first)
            first = false;
        else
            out << ", ";
        out << "\"";
        serialize_string(out, i.first);
        out << "\": ";
        i.second.serialize(out);
    }
    out << "}";
}

void json_bool::serialize(std::ostream &s) const
{
    if (_value)
        s << "true";
    else
        s << "false";
}

void json_double::serialize(std::ostream &s) const
{
    s << _value;
}

void json_array::serialize(std::ostream &s) const
{
    serialize_array(s, _value);
}

void json_int::serialize(std::ostream &s) const
{
    s << _value;
}

void json_null::serialize(std::ostream &s) const
{
    s << "null";
}

void json_json::serialize(std::ostream &s) const
{
    _value.serialize(s);
}

void json_string::serialize(std::ostream &s) const
{
    s << "\"";
    serialize_string(s, _value);
    s<< "\"";
}

}}
