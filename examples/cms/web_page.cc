#include "web_page.h"
#include "redis_context.h"

#include <hiredis/hiredis.h>

web_page::web_page(redis_context &context)
    : _context(context)
{
}

std::vector<std::string> web_page::get_page_titles()
{
    std::vector<std::string> res;
    std::function<void(redisContext *)> fun = [&res](redisContext *context) {
        redisReply *resp = (redisReply *) redisCommand(context, "zrange pagenames 0 -1");
        if (resp == nullptr) {
            return;
        }
        if (resp->type == REDIS_REPLY_STRING) {
            res.push_back({resp->str, resp->len});
        } else if (resp->type == REDIS_REPLY_ARRAY) {
            for (size_t i = 0; i < resp->elements; ++i) {
                res.push_back({resp->element[i]->str, resp->element[i]->len});
            }
        }
    };
    _context(fun);
    return res;
}
