#pragma once
#include <functional>
#include <string>
#include <stack>
#ifdef __APPLE__
# include <boost/thread/mutex.hpp>
# include <boost/thread/condition_variable.hpp>
# define STD boost
#else
# include <thread>
# define STD std
#endif

struct redisContext;

class redis_context {
public:
    redis_context(const std::string& ip, int port, unsigned max_cons = 32);
    ~redis_context();
public: //access
    void operator ()(const std::function<void(redisContext*)>& fun) {
        redisContext *context = get_context();
        fun(context);
        return_context(context);
    }
private:
    redisContext *get_context();
    void return_context(redisContext *context);
private:
    std::stack<redisContext *> _contexts;
    std::string _ip;
    int _port;
    unsigned _cons;
    STD::mutex _mutex;
    STD::condition_variable _cond;
};
