#include <hiredis/hiredis.h>
#include "redis_context.h"
#include "web_page.h"

#include <nodecpp/server/http_server.h>
#include <nodecpp/json/json.h>

using namespace nodecpp::server;
using namespace nodecpp::json;

int main(int argc, const char **argv) {
    redis_context context("127.0.01", 6379);
    web_page pages(context);
    http_server server(8080);
    server["/pagenames"] = [&pages](const http_request &req, std::ostream &s) {
        json names = pages.get_page_titles();
        http_response resp(200, "OK");
        resp["Content-Type"] = "application/json";
        s << resp;
        s << names;
    };
    server["/public.*"] = file_handler("/usr/share/doc/redis-doc/doc", "/public");
    server.main_loop();
}
