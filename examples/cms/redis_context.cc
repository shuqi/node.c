#include "redis_context.h"
#include <hiredis/hiredis.h>

redis_context::redis_context(const std::string &ip, int port, unsigned max_cons)
    : _ip(ip), _port(port), _cons(max_cons)
{
}

redis_context::~redis_context()
{
    STD::lock_guard<STD::mutex> lock(_mutex);
    while (!_contexts.empty()) {
        delete _contexts.top();
        _contexts.pop();
    }
}

redisContext *redis_context::get_context()
{
    STD::unique_lock<STD::mutex> lock(_mutex);
    if (_contexts.empty() && _cons) {
        --_cons;
        return redisConnect(_ip.c_str(), _port);
    }
    while (_contexts.empty()) {
        _cond.wait(lock);
    }
    redisContext *res = _contexts.top();
    _contexts.pop();
    lock.unlock();
    return res;
}

void redis_context::return_context(redisContext *context)
{
    STD::lock_guard<STD::mutex> lock(_mutex);
    _contexts.push(context);
    _cond.notify_one();
}
