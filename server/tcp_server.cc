/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "tcp_server.h"
#include "socket.h"

#include <nodecpp/server/tcp_server.h>
#include <memory>
#ifdef __APPLE__
# include <boost/thread.hpp>
#else
# include <thread>
#endif
#include <event2/event.h>

namespace nodecpp { namespace server {

tcp_server::tcp_server(short port, std::function<void(std::iostream&)> handler)
    : _impl(new nodecppimpl::server::tcp_server(port, handler)) {}

void tcp_server::main_loop()
{
    _impl->main_loop();
}

}}

namespace nodecppimpl { namespace server {

static const int BUF_SIZE = 1024;

socket_streambuf::socket_streambuf(tcp_socket& aSocket)
    : _socket(aSocket)
{
    _input_buffer = new char_type[BUF_SIZE];
    _output_buffer = new char_type[BUF_SIZE];
    setg(_input_buffer, _input_buffer, _input_buffer);
    setp(_output_buffer, _output_buffer + BUF_SIZE);
}

socket_streambuf::~socket_streambuf()
{
    delete[] _input_buffer;
    delete[] _output_buffer;
}

int socket_streambuf::sync()
{
#if 0
    std::cout << "sending:" << std::endl << std::string(pbase(), pptr() - pbase()) << std::endl;
#endif
    _socket.send(pbase(), pptr() - pbase());
    setp(_output_buffer, _output_buffer + BUF_SIZE);
    return 0;
}

int socket_streambuf::underflow() {
    int size = _socket.recv(_input_buffer, BUF_SIZE);
    if (size == 0)
        return EOF;
    setg(_input_buffer, _input_buffer, _input_buffer + size);
    return _input_buffer[0];
}

int socket_streambuf::overflow(int c)
{
#if 0
    std::cout << "sending:" << std::endl << std::string(pbase(), pptr() - pbase()) << std::endl;
#endif
    _socket.send(pbase(), pptr() - pbase());
    setp(_output_buffer, _output_buffer + BUF_SIZE);
    sputc(c);
    return 0;
}

static void exec_request(tcp_socket *socket, std::function<void(std::iostream& stream)> fun)
{
    std::unique_ptr<std::streambuf> streambuf(new socket_streambuf(*socket));
    std::iostream stream(streambuf.get());
    fun(stream);
    stream.sync();
    socket->close();
    delete socket;
}

tcp_server::tcp_server(short port, decltype(_handler) handler) : _port(port), _handler(handler) {}

void tcp_server::main_loop() {
    try {
        tcp_server_socket sock(_port);
        for (;;) {
            auto socket = sock.accept();
#ifdef __APPLE__
            boost::thread t([socket, _handler]() { exec_request(socket, _handler); });
#else
            std::thread t([socket, _handler]() { exec_request(socket, _handler); });
#endif
            t.detach();
        }
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

}}
