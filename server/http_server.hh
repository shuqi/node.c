/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "tcp_server.h"

#include <nodecpp/server/http_server.h>

#include <string>
#include <map>
#include <functional>
#include <regex>

namespace nodecpp { namespace server {
class http_request;
class http_server;
}}

namespace nodecppimpl { namespace server {

class http_request {
    friend class http_server;
    friend class nodecpp::server::http_request;
    friend class parse_status;
private:
    std::string _method;
    std::string _path;
    std::string _query_string;
    std::string _url;
    std::map<std::string, std::string> _headers;
public:
    std::string operator[] (const std::string& key) const;
    const std::string &method() const { return _method; }
    const std::string &path() const { return _path; }
    const std::string &query_string() const { return _query_string; };
};

class http_server
{
    friend class nodecpp::server::http_server;
public:
    std::function<void (const nodecpp::server::http_request& request, std::iostream& stream)> default_handler;
private:
    std::map<std::string, decltype(default_handler), std::greater<std::string> > _handlers;
    tcp_server *_server;
private:
    void main_handler(std::iostream& stream);
    http_server();
public:
    http_server(short port, decltype(default_handler) def_handler);
    http_server(short port);
    ~http_server();
    void main_loop();
};

class file_handler {
private:
    std::map<std::string, std::string> _types;
    std::string _root_dir;
    std::string _pattern;
public:
    file_handler(const std::string &root_dir, const std::string &pattern);
    std::string &operator [](const std::string &suffix);
    void operator ()(const nodecpp::server::http_request &req, std::ostream &s);
};

}} //namespaces

