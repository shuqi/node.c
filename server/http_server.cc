/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "http_server.hh"

#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/regex.hpp>
#include <http_parser.h>

namespace nodecpp { namespace server {

http_request::http_request(nodecppimpl::server::http_request *impl)
    : _impl(impl) {}

const std::string& http_request::method() const {
    return _impl->_method;
}

const std::string& http_request::path() const {
    return _impl->_path;
}

std::string http_request::operator [](const std::string &key) const {
    return (*_impl)[key];
}

http_server::http_server(http_server &&other) :_impl(other._impl)
{
}

http_server::http_server(short port)
    : _impl(new nodecppimpl::server::http_server(port)) {}

http_server::http_server(short port, std::function<void (const http_request &, std::iostream &)> def_handler)
    : _impl(new nodecppimpl::server::http_server(port, def_handler)) {}

http_server::~http_server()
{
    delete _impl;
}

http_server& http_server::operator =(const std::function<void (const http_request&, std::iostream& stream)>& def_handler)
{
    _impl->default_handler = def_handler;
}

std::function<void (const http_request&, std::iostream& stream)>& http_server::operator [](const std::string& regex)
{
    return _impl->_handlers[regex];
}

void http_server::main_loop() {
    _impl->main_loop();
}

void http_response::serialize(std::ostream &s) const
{
    s << "HTTP/1.1 " << _status << " " << _msg << std::endl;
    for (auto i : _headers) {
        s << i.first << ": " << i.second << std::endl;
    }
    s << std::endl;
}

file_handler::file_handler(const std::string &root_dir, const std::string &pattern)
    : _impl(new nodecppimpl::server::file_handler(root_dir, pattern)) {}

file_handler::file_handler(file_handler &&other)
    : _impl(other._impl)
{
    other._impl = nullptr;
}

file_handler::file_handler(const file_handler &other)
    : _impl(new nodecppimpl::server::file_handler(*other._impl)) {}

file_handler::~file_handler()
{
    delete _impl;
}

file_handler &file_handler::operator =(const file_handler &other)
{
    delete _impl;
    _impl = new nodecppimpl::server::file_handler(*other._impl);
    return *this;
}

file_handler &file_handler::operator =(file_handler &&other)
{
    _impl = other._impl;
    other._impl = nullptr;
    return *this;
}

std::string &file_handler::operator [](const std::string &str)
{
    return (*_impl)[str];
}

void file_handler::operator ()(const http_request &req, std::ostream &s)
{
    (*_impl)(req, s);
}

}} //namespaces

namespace std {

ostream& operator <<(ostream& s, const nodecpp::server::http_response& r)
{
    r.serialize(s);
    return s;
}

}

namespace nodecppimpl { namespace server {

class parse_status : public std::streambuf {
    friend class http_server;
private: //types
    enum state {
        NOT_STARTED,
        STARTED,
        PATH,
        QUERY_STRING,
        URL,
        HEADER_FIELD,
        HEADER_VALUE,
        BODY,
        COMPLETE
    };
private:
    http_request *_request;
    http_parser *_parser;
    http_parser_settings *_settings;
    std::iostream &_stream;
    bool _finish;
    enum state _state;
    std::stringstream _curr_string;
    std::pair<std::string, std::string> _curr_header;
    char _idata[1024];
    char _odata[1024];
protected: // from streambuf
    virtual int underflow();
    virtual int overflow(int c);
    virtual int sync();
public:
    parse_status(http_request *request, http_parser *parser, http_parser_settings *settings, std::iostream &stream)
        : _request(request), _parser(parser), _settings(settings),
          _stream(stream), _finish(false), _state(NOT_STARTED)
    {
        setp(_odata, _odata + 1024);
    }
    inline bool finish() const { return _finish; }
public: //handler functions
    static int on_message_begin(http_parser *parser);
    static int on_path(http_parser *parser, const char *at, size_t length);
    static int on_query_string(http_parser *parser, const char *at, size_t length);
    static int on_url(http_parser *parser, const char *at, size_t length);
    static int on_header_field(http_parser *parser, const char *at, size_t length);
    static int on_header_value(http_parser *parser, const char *at, size_t length);
    static int on_headers_complete(http_parser *parser);
    static int on_body(http_parser *parser, const char *at, size_t length);
    static int on_message_complete(http_parser *parser);
};

int parse_status::sync()
{
    _stream.write(pbase(), epptr() - pbase());
    setp(_odata, _odata + 1024);
    return 0;
}

int parse_status::overflow(int c)
{
    _stream.write(pbase(), epptr() - pbase());
    _odata[0] = (char)c;
    setp(_odata, _odata + 1024);
    return EOF+1;
}

int parse_status::underflow()
{
    if (_state == COMPLETE) {
        return EOF;
    }
    char buf[1024];
    _stream.get(buf, 1024);
    if (_stream.gcount() == 0) {
        _state = COMPLETE;
        return EOF;
    }
    http_parser_execute(_parser, _settings, buf, _stream.gcount());
    return _idata[0];
}

int parse_status::on_message_begin(http_parser *parser)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    status->_state = STARTED;
    return 0;
}

int parse_status::on_path(http_parser *parser, const char *at, size_t length) {
    parse_status *status = static_cast<parse_status *>(parser->data);
    switch (status->_state) {
    case PATH:
        break;
    case QUERY_STRING:
        status->_request->_query_string = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case URL:
        status->_request->_url = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    default:
        status->_curr_string.str("");
    }
    status->_curr_string << std::string(at, length);
    status->_state = PATH;
    return 0;
}

int parse_status::on_query_string(http_parser *parser, const char *at, size_t length)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    switch (parser->state) {
    case PATH:
        status->_request->_path = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case QUERY_STRING:
        break;
    case URL:
        status->_request->_path = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    default:
        status->_curr_string.str("");
    }
    status->_curr_string << std::string(at, length);
    status->_state = QUERY_STRING;
    return 0;
}

int parse_status::on_url(http_parser *parser, const char *at, size_t length)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    switch (parser->state) {
    case PATH:
        status->_request->_path = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case QUERY_STRING:
        status->_request->_query_string = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case URL:
        status->_request->_path = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    default:
        status->_curr_string.str("");
    }
    status->_curr_string << std::string(at, length);
    status->_state = URL;
    return 0;
}

int parse_status::on_header_field(http_parser *parser, const char *at, size_t length)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    switch (parser->state) {
    case HEADER_FIELD:
        break;
    case HEADER_VALUE:
        status->_curr_header.second = status->_curr_string.str();
        status->_request->_headers.insert(status->_curr_header);
        status->_curr_string.str("");
        break;
    case PATH:
        status->_request->_path = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case QUERY_STRING:
        status->_request->_query_string = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    case URL:
        status->_request->_url = status->_curr_string.str();
        status->_curr_string.str("");
        break;
    default:
        status->_curr_string.str("");
    }
    status->_curr_string << std::string(at, length);
    status->_state = HEADER_FIELD;
    return 0;
}

int parse_status::on_header_value(http_parser *parser, const char *at, size_t length)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    if (parser->state == HEADER_FIELD) {
        status->_curr_header.first = status->_curr_string.str();
        status->_curr_string.str("");
    }
    status->_curr_string << std::string(at, length);
    status->_state = HEADER_VALUE;
    return 0;
}

int parse_status::on_headers_complete(http_parser *parser)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    status->_curr_header.second = status->_curr_string.str();
    status->_finish = true;
    return 0;
}

int parse_status::on_body(http_parser *parser, const char *at, size_t length)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    assert(length < 1024);
    memcpy(status->_idata, at, length*sizeof(char));
    status->setg(status->_idata, status->_idata, status->_idata + length);
    status->_state = BODY;
    return 0;
}

int parse_status::on_message_complete(http_parser *parser)
{
    parse_status *status = static_cast<parse_status *>(parser->data);
    status->_state = COMPLETE;
    return 0;
}

class connection_status {
private:
    bool _close;
public:
    connection_status() : _close(false) {}
    inline bool close() const { return _close; }
    inline void closeConnection() { _close = true; }
};

std::string http_request::operator [](const std::string &key) const
{
    auto i = _headers.find(key);
    if (i == _headers.end()) {
        return nullptr;
    }
    return i->second;
}

http_server::http_server(short port)
{
    _server = new tcp_server(port, [this](std::iostream& stream) { main_handler(stream); });
    default_handler = [](const nodecpp::server::http_request& request, std::ostream& stream) {
        stream << "HTTP/1.1 440 Not Found" << std::endl;
        stream << "Content-Type: text/html; charset=utf-8" << std::endl << std::endl;
        stream << "<html><head><title>Not Found</title></head><body><h1>404 Not Found</1>The requested resource could not be found.</body></html>" << std::endl;
    };
}

http_server::http_server(short port, decltype(default_handler) def_handler)
    : default_handler(def_handler)
{
    _server = new tcp_server(port, [this](std::iostream& stream) { main_handler(stream); });
}

http_server::~http_server()
{
    delete _server;
}

void http_server::main_handler(std::iostream &stream)
{
    connection_status status;
    http_parser_settings settings;
    settings.on_message_begin = &parse_status::on_message_begin;
    settings.on_path = &parse_status::on_path;
    settings.on_query_string = &parse_status::on_query_string;
    settings.on_url = &parse_status::on_url;
    settings.on_header_field = &parse_status::on_header_field;
    settings.on_header_value = &parse_status::on_header_value;
    settings.on_headers_complete = &parse_status::on_headers_complete;
    settings.on_body = &parse_status::on_body;
    settings.on_message_complete = &parse_status::on_message_complete;
    while (!status.close() && stream) {
        http_request request;
        http_parser parser;
        parse_status p_status(&request, &parser, &settings, stream);
        http_parser_init(&parser, HTTP_REQUEST);
        parser.data = &p_status;
        char buf[1024];
        while (!p_status.finish() && stream.get(buf, 1024)) {
            http_parser_execute(&parser, &settings, buf, stream.gcount());
        }
        std::iostream wrapped_stream(&p_status);
        nodecpp::server::http_request req(&request);
        for (auto i : _handlers) {
            if (boost::regex_match(request._path, boost::regex(i.first))) {
                i.second(req, wrapped_stream);
                return;
            }
        }
        default_handler(req, stream);
    }
}

void http_server::main_loop()
{
    _server->main_loop();
}

file_handler::file_handler(const std::string &root_dir, const std::string &pattern)
    : _root_dir(root_dir), _pattern(pattern) {}

std::string &file_handler::operator [](const std::string &str)
{
    return _types[str];
}

void file_handler::operator ()(const nodecpp::server::http_request& req, std::ostream &s)
{
    std::string p = req.path();
    if (p.substr(0, _pattern.size()) != _pattern)
        throw nodecpp::server::file_not_found("The pattern " + _pattern  + "does not match" + p);
    std::fstream in(_root_dir + p.substr(_pattern.size()), std::ios_base::in);
    if (!in.good())
        throw nodecpp::server::file_not_found("Could not find " + _root_dir + p.substr(_pattern.size()));
    char buffer[1024];
    while (in.read(buffer, 1024)) {
        std::string str(buffer, in.gcount());
        s << str;
    }
}

}}
