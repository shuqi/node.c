/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "socket.h"
#include <sstream>
#ifdef __APPLE__
# include <boost/thread/mutex.hpp>
# define STD boost
#else
# include <mutex>
# define STD std
#endif

#if defined WIN32 || defined WINCE
#include <winsock2.h>
  typedef int socklen_t;
  typedef char raw_type;

#ifdef WIN32
static bool initialized = false;
#endif
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>

#include <cstring>
#include <cstdlib>

  typedef void raw_type;
#endif

#include <errno.h>

namespace nodecppimpl {

// Function to fill in address structure given an address and port
static void fillAddr(const std::string &address, unsigned short port,
                     sockaddr_in &addr) {
  // we have to lock calls to gethostbyname because
  // it's not thread-safe
  static STD::mutex mutex;
  memset(&addr, 0, sizeof(addr));  // Zero out address structure
  addr.sin_family = AF_INET;       // Internet address

  STD::lock_guard<STD::mutex> lock(mutex);
  hostent *host;  // Resolve name
  if ((host = gethostbyname(address.c_str())) == NULL) {
    // strerror() will not work for gethostbyname() and hstrerror()
    // is supposedly obsolete
    throw socket_exception("Failed to resolve name (gethostbyname())");
  }
  addr.sin_addr.s_addr = *((unsigned long *) host->h_addr_list[0]);

  addr.sin_port = htons(port);     // Assign port in network byte order
}

// Socket Code

socket::socket(int aType, int aProtocol)
{
#ifdef WIN32
  if (!initialized) {
    WORD wVersionRequested;
    WSADATA wsaData;

    wVersionRequested = MAKEWORD(2, 0);              // Request WinSock v2.0
    if (WSAStartup(wVersionRequested, &wsaData) != 0) {  // Load WinSock DLL
      throw socket_exception("Unable to load WinSock DLL");
    }
    initialized = true;
  }
#endif

  // Make a new socket
  _descriptor = ::socket(PF_INET, aType, aProtocol);
  if (_descriptor == INVALID_SOCKET) {
      throw socket_exception("Socket creation failed (socket())");
  }
}

socket::socket(SOCKET aDescriptor)
{
  this->_descriptor = aDescriptor;
}

socket::~socket() {
  close();
}

void
socket::close() {
#ifdef WIN32
  ::closesocket(theDescriptor);
#else
  ::close(_descriptor);
#endif
}

std::string socket::get_local_address() {
  sockaddr_in addr;
  unsigned int addr_len = sizeof(addr);

  if (getsockname(_descriptor, (sockaddr *) &addr, (socklen_t *) &addr_len) < 0) {
    throw socket_exception("Fetch of local address failed (getsockname())");
  }
  return inet_ntoa(addr.sin_addr);
}

unsigned short socket::get_local_port() {
  sockaddr_in addr;
  unsigned int addr_len = sizeof(addr);

  if (getsockname(_descriptor, (sockaddr *) &addr, (socklen_t *) &addr_len) < 0) {
    throw socket_exception("Fetch of local port failed (getsockname())");
  }
  return ntohs(addr.sin_port);
}

void socket::set_local_port(unsigned short localPort) {
  // Bind the socket to its port
  sockaddr_in localAddr;
  memset(&localAddr, 0, sizeof(localAddr));
  localAddr.sin_family = AF_INET;
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  localAddr.sin_port = htons(localPort);
  int opt = 1;
  setsockopt(_descriptor, SOL_SOCKET,SO_REUSEADDR, (char *)&opt, (socklen_t)sizeof(opt));
  if (bind(_descriptor, (sockaddr *) &localAddr, sizeof(sockaddr_in)) < 0) {
    std::stringstream lMsg;
    lMsg << "Set of local port failed: " << localPort;
    throw socket_exception(lMsg.str());
  }
}

void socket::set_local_address_and_port(const std::string &localAddress,
    unsigned short localPort) {
  // Get the address of the requested host
  sockaddr_in localAddr;
  fillAddr(localAddress, localPort, localAddr);
  int opt = 1;
  setsockopt(_descriptor, SOL_SOCKET,SO_REUSEADDR, (char *)&opt, (socklen_t)sizeof(opt));
  if (bind(_descriptor, (sockaddr *) &localAddr, sizeof(sockaddr_in)) < 0) {
    throw socket_exception("Set of local address and port failed (bind())");
  }
}

void socket::clean_up() {
#ifdef WIN32
    if (WSACleanup() != 0) {
      throw socket_exception("WSACleanup() failed");
    }
#endif
}

unsigned short socket::resolve_service(const std::string &service,
                                      const std::string &protocol) {
  struct servent *serv;        /* Structure containing service information */

  if ((serv = getservbyname(service.c_str(), protocol.c_str())) == NULL)
    return atoi(service.c_str());  /* Service is port number */
  else
    return ntohs(serv->s_port);    /* Found port (network byte order) by name */
}

// CommunicatingSocket Code

communicating_socket::communicating_socket(int type, int protocol):
  socket(type, protocol) {
}

communicating_socket::communicating_socket(SOCKET newConnSD) : socket(newConnSD) {
}

void communicating_socket::connect(const std::string &foreignAddress,
    unsigned short foreignPort) {
  // Get the address of the requested host
  sockaddr_in destAddr;
  fillAddr(foreignAddress, foreignPort, destAddr);

  // Try to connect to the given port
  if (::connect(_descriptor, (sockaddr *) &destAddr, sizeof(destAddr)) < 0) {
    std::stringstream lMsg;
    lMsg << "Connection @" << foreignAddress << ":" << foreignPort << " failed.";
    throw socket_exception( lMsg.str());
  }
}

void communicating_socket::send(const void *buffer, int bufferLen)
{
  if (::send(_descriptor, (raw_type *) buffer, bufferLen, 0) < 0) {
    throw socket_exception("Send failed (send())");
  }
}

int communicating_socket::recv(void *buffer, int bufferLen) {
  int rtn;
  if ((rtn = ::recv(_descriptor, (raw_type *) buffer, bufferLen, 0)) < 0) {
    throw socket_exception("Received failed (recv())");
  }

  return rtn;
}

std::string communicating_socket::get_foreign_address() {
  sockaddr_in addr;
  unsigned int addr_len = sizeof(addr);

  if (getpeername(_descriptor, (sockaddr *) &addr,(socklen_t *) &addr_len) < 0) {
    throw socket_exception("Fetch of foreign address failed (getpeername())");
  }
  return inet_ntoa(addr.sin_addr);
}

unsigned short communicating_socket::get_foreign_port() {
  sockaddr_in addr;
  unsigned int addr_len = sizeof(addr);

  if (getpeername(_descriptor, (sockaddr *) &addr, (socklen_t *) &addr_len) < 0) {
    throw socket_exception("Fetch of foreign port failed (getpeername())");
  }
  return ntohs(addr.sin_port);
}

// TCPSocket Code

tcp_socket::tcp_socket(): communicating_socket(SOCK_STREAM, IPPROTO_TCP) {
}

tcp_socket::tcp_socket(const std::string &foreignAddress, unsigned short foreignPort): communicating_socket(SOCK_STREAM, IPPROTO_TCP) {
  connect(foreignAddress, foreignPort);
}

tcp_socket::tcp_socket(SOCKET newConnSD) : communicating_socket(newConnSD) {
}

// TCPServerSocket Code

tcp_server_socket::tcp_server_socket(unsigned short localPort, int queueLen): socket(SOCK_STREAM, IPPROTO_TCP) {
  set_local_port(localPort);
  setListen(queueLen);
}

tcp_server_socket::tcp_server_socket(const std::string &localAddress,
    unsigned short localPort, int queueLen): socket(SOCK_STREAM, IPPROTO_TCP) {
  set_local_address_and_port(localAddress, localPort);
  setListen(queueLen);
}

tcp_socket *tcp_server_socket::accept() {
  SOCKET newConnSD = ::accept(_descriptor, NULL, 0);
  if (newConnSD == INVALID_SOCKET) {
    throw socket_exception("Accept failed (accept())");
  }

  return new tcp_socket(newConnSD);
}

void tcp_server_socket::setListen(int queueLen) {
  if (listen(_descriptor, queueLen) < 0) {
    throw socket_exception("Set listening socket failed (listen())");
  }
}
}
