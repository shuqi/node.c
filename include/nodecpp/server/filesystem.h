#pragma once
#include <iostream>

namespace nodecpp { namespace server {

class http_request;

namespace impl {
class filesystem_handler_impl;
}

class filesystem_handler {
public:
    filesystem_handler(const std::string& root_path);
    filesystem_handler(std::string&& root_path);
    filesystem_handler(const filesystem_handler& other);
    filesystem_handler(filesystem_handler&& other);
    ~filesystem_handler();
    void operator ()(const http_request& request, std::ostream& stream);
private:
    impl::filesystem_handler_impl *_impl;
};

}}
