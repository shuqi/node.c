/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <functional>
#include <iostream>

namespace nodecppimpl {
namespace server {
class tcp_server;
}
}

namespace nodecpp {
namespace server {

class tcp_server {
private:
    nodecppimpl::server::tcp_server *_impl;
public:
    tcp_server(short port, std::function<void(std::iostream&)> handler);
    void main_loop();
};

}
}
