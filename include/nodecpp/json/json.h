/*
 * Copyright 2011 Markus Pilman <markus@pilman.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <string>
#include <tuple>
#include <vector>

namespace nodecpp {
namespace json {
class json;
class json_value;
}}

namespace nodecppimpl {
namespace json {
class json;
class json_value;
}}

namespace nodecpp {
template<typename Pub, typename Impl>
struct unmarshaller;
}

namespace nodecpp { namespace json {

class json_exception : public std::exception {
public:
    json_exception(const std::string& msg) : _msg(msg) {}
    json_exception(std::string&& msg) : _msg(msg) {}
    json_exception(const json_exception& ex) : _msg(ex._msg) {}
    json_exception& operator =(const json_exception& ex) { _msg = ex._msg; return *this; }
    virtual ~json_exception() throw() {}
    virtual const char *what() const throw() { return _msg.c_str(); }
protected:
    std::string _msg;
};

class wrong_type_exception : json_exception {
public:
    wrong_type_exception(const std::string& msg) : json_exception(msg) {}
    wrong_type_exception(std::string&& msg) : json_exception(msg) {}
    wrong_type_exception(const wrong_type_exception& ex) : json_exception(ex._msg) {}
    wrong_type_exception& operator =(const wrong_type_exception& ex) { _msg = ex._msg; return *this; }
};

class json;

class json_value {
    friend class nodecppimpl::json::json;
public: //type
    typedef enum {
        STRING,
        INT,
        DOUBLE,
        JSON,
        BOOL,
        ARRAY,
        NULLPTR
    } type;
public: //construction - destruction
    json_value(const std::string& str);
    json_value(const char* str);
    json_value(long long i);
    json_value(double d);
    json_value(const json& js);
    json_value(json&& js);
    json_value();
    json_value(json_value&& other);
    json_value(const json_value& other);
    json_value(const std::vector<json_value>& arr);
    json_value(std::vector<json_value>&& arr);
    json_value(bool boolean);
    ~json_value();
public: //assignment
    json_value& operator =(const std::string& value);
    json_value& operator =(const char* value);
    json_value& operator =(long long value);
    json_value& operator =(double value);
    json_value& operator =(const json& value);
    json_value& operator =(const json_value& other);
    json_value& operator =(const std::vector<json_value>& other);
    json_value& operator =(bool& other);
public: //comparison
    bool operator ==(const json_value& other) const;
    bool operator ==(const std::string& other) const;
    bool operator ==(const char* other) const;
    bool operator ==(const json& other) const;
    bool operator ==(long long other) const;
    bool operator ==(double other) const;
    bool operator ==(bool other) const;
    bool operator ==(const std::vector<json_value>& other) const;
    bool operator ==(decltype(nullptr) other) const;

    bool operator !=(const json_value& other) const;
    bool operator !=(const std::string& other) const;
    bool operator !=(const char* other) const;
    bool operator !=(const json& other) const;
    bool operator !=(long long other) const;
    bool operator !=(bool other) const;
    bool operator !=(double other) const;
    bool operator !=(decltype(nullptr) other) const;
public: //converting
    operator std::string();
    operator long long();
    operator double();
    operator json();
    operator bool();
public:
    void get_string(std::string&) const;
    long long get_int() const;
    double get_double() const;
    void get_json(json&) const;
    bool get_bool() const;
    type get_type() const;
    const std::vector<nodecpp::json::json_value>& get_array() const;
    std::vector<nodecpp::json::json_value>& get_array();
    void serialize(std::ostream& s) const;
private:
    json_value(nodecppimpl::json::json_value *impl);
    nodecppimpl::json::json_value *_impl;
};

class json {
    friend class nodecppimpl::json::json;
    friend class json_value;
    friend struct unmarshaller<json, nodecppimpl::json::json>;
private:
    json(const nodecppimpl::json::json &other);
    json(nodecppimpl::json::json *other);
public:
    json();
    json(std::initializer_list<std::tuple<std::string, json_value> > objs);
    json(std::initializer_list<long long> objs);
    json(std::initializer_list<int> objs);
    json(std::initializer_list<double> objs);
    json(std::initializer_list<std::string> objs);
    json(std::initializer_list<const char*> objs);
    json(std::initializer_list<bool> objs);
    json(std::vector<std::tuple<std::string, json_value> > objs);
    json(std::vector<json_value>&& objs);
    json(const std::vector<json_value>& objs);
    json(const json& other);
    json(const std::vector<std::string> &str_arr);
    json& operator =(const json& other);
    ~json();
public: //access elements
    json_value& operator [](const std::string& key);
    json_value& operator [](unsigned int pos);
public: //parse and serialize
    void serialize(std::ostream& stream) const;
    static json parse(std::istream& stream);
private:
    nodecppimpl::json::json *_impl;
};

}}

std::tuple<std::string, nodecpp::json::json_value> operator <<(const std::string&, const nodecpp::json::json_value& value);

std::ostream& operator <<(std::ostream&, const nodecpp::json::json&);

